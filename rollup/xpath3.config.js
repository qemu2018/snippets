import {nodeResolve} from '@rollup/plugin-node-resolve';
import {terser} from 'rollup-plugin-terser';
import cleanup from 'rollup-plugin-cleanup';

const beautify = () => cleanup({
  exclude: ['**/hide-if-matches-xpath3-dependency*.js'],
  comments: 'none',
  maxEmptyLines: 1
});

export default [
  {
    input: './bundle/xpath3.js',
    plugins: [
      nodeResolve(),
      beautify()
    ],
    output: {
      esModule: false,
      file: './dist/isolated-xpath3.source.js',
      format: 'commonjs'
    }
  },
  {
    input: './bundle/xpath3.js',
    plugins: [
      nodeResolve(),
      beautify(),
      terser()
    ],
    output: {
      esModule: false,
      file: './dist/isolated-xpath3.js',
      format: 'commonjs'
    }
  }
];
