/*!
 * This file is part of eyeo's Anti-Circumvention Snippets module (@eyeo/snippets),
 * Copyright (C) 2006-present eyeo GmbH
 * 
 * @eyeo/snippets is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 * 
 * @eyeo/snippets is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with @eyeo/snippets.  If not, see <http://www.gnu.org/licenses/>.
 */

import {$$} from "../utils/dom.js";
import $ from "../$.js";

import {getDebugger} from "../introspection/log.js";
import {initQueryAndApply} from "../utils/dom.js";

let {isNaN, MutationObserver, parseInt, parseFloat, setTimeout} = $(window);

/**
 * Skips video
 * @alias module:content/snippets.skip-video
 *
 * @param {string} playerSelector The CSS or the XPath selector to the
 * <video> element in the page.
 * @param {string} xpathCondition The XPath selector that will be used to
 * know when to trigger the skipping logic.
 * @param {?string} skipTo Determines the time of the video to skip to.
 * Skips to the end if value is negative or zero.
 * Fast forwards video with the given value if positive.
 * @param {?string} maxAttempts If the video is not fully loaded by the
 * time the xpath condition is met; there is a retry mechanism in the snippet.
 * maxAttempts parameter will determine the maximum number of attemps
 * the snippet should do before giving up.
 * @param {?string} retryMs The snippet will try to skip the video
 * once every retryMs interval.
 */
export function skipVideo(playerSelector, xpathCondition,
                          skipTo = -0.1, maxAttempts = 10,
                          retryMs = 10) {
  const skipToNum = parseFloat(skipTo || -0.1);
  const maxAttemptsNum = parseInt(maxAttempts || 10, 10);
  const retryMsNum = parseInt(retryMs || 10, 10);
  const debugLog = getDebugger("skip-video");
  const queryAndApply = initQueryAndApply(`xpath(${xpathCondition})`);

  const callback = (retryCounter = 0) => {
    queryAndApply(node => {
      debugLog("Matched: ", node, " for selector: ", xpathCondition);
      debugLog("Running video skipping logic.");
      const video = $$(playerSelector)[0];
      while (isNaN(video.duration) && retryCounter < maxAttemptsNum) {
        setTimeout(() => {
          const attempt = retryCounter + 1;
          debugLog("Running video skipping logic. Attempt: ", attempt);
          callback(attempt);
        }, retryMsNum);
        return;
      }
      if (!isNaN(video.duration)) {
        video.muted = true;
        debugLog("Muted video...");
        // If skipTo is zero or negative, skip to the end of the video
        // If skipTo is positive, skip forward for the given time.
        skipToNum <= 0 ?
          video.currentTime = video.duration + skipToNum :
          video.currentTime += skipToNum;
        debugLog("Skipped duration...");
        video.paused && video.play();
      }
    });
  };

  const mo = new MutationObserver(callback);
  mo.observe(
    document, {characterData: true, childList: true, subtree: true});
  callback();
}
