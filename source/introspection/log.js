/*!
 * This file is part of eyeo's Anti-Circumvention Snippets module (@eyeo/snippets),
 * Copyright (C) 2006-present eyeo GmbH
 * 
 * @eyeo/snippets is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 * 
 * @eyeo/snippets is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with @eyeo/snippets.  If not, see <http://www.gnu.org/licenses/>.
 */

import $ from "../$.js";
import {bind} from "proxy-pants/function";

import {debug} from "./debug.js";

const {console} = $(window);

export const noop = () => {};

/**
 * Logs its arguments to the console.
 *
 * This may be used for testing and debugging.
 *
 * @alias module:content/snippets.log
 *
 * @param {...*} [args] The arguments to log.
 *
 * @since Adblock Plus 3.3
 */
export function log(...args) {
  if (debug())
    $(args).unshift("%c DEBUG", "font-weight: bold");

  console.log(...args);
}

/**
 * Returns a no-op if debugging mode is off, returns a bound log otherwise.
 * @param {string} name the debugger name (first logged value)
 * @returns {function} either a no-op function or the logger one
 */
export function getDebugger(name) {
  return bind(debug() ? log : noop, null, name);
}
