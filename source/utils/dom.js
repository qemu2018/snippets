/*!
 * This file is part of eyeo's Anti-Circumvention Snippets module (@eyeo/snippets),
 * Copyright (C) 2006-present eyeo GmbH
 * 
 * @eyeo/snippets is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 * 
 * @eyeo/snippets is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with @eyeo/snippets.  If not, see <http://www.gnu.org/licenses/>.
 */

/* global checkElement:readonly */

import $ from "../$.js";
import {apply, bind} from "proxy-pants/function";

import {libEnvironment} from "../environment.js";

let {
  document,
  getComputedStyle,
  isExtensionContext,
  variables,
  Array,
  MutationObserver,
  Object,
  XPathEvaluator,
  XPathExpression,
  XPathResult
} = $(window);

// ensures that $$ is bound only in environments where document exists
let {querySelectorAll} = document;
export let $$ = querySelectorAll && bind(querySelectorAll, document);

// make `new XPathExpression()` operations safe
const {assign, setPrototypeOf} = Object;

class $XPathExpression extends XPathExpression {
  evaluate(...args) {
    return setPrototypeOf(
      apply(super.evaluate, this, args),
      XPathResult.prototype
    );
  }
}

class $XPathEvaluator extends XPathEvaluator {
  createExpression(...args) {
    return setPrototypeOf(
      apply(super.createExpression, this, args),
      $XPathExpression.prototype
    );
  }
}

/**
 * Hides an HTML element by setting its `style` attribute to
 * `display: none !important`.
 *
 * @param {HTMLElement} element The HTML element to hide.
 * @private
 * @returns {bool} true if element has been newly hidden,
 * false if the element was already hidden and no action was taken.
 */
export function hideElement(element) {
  if (variables.hidden.has(element))
    return false;

  notifyElementHidden(element);

  variables.hidden.add(element);

  let {style} = $(element);
  let $style = $(style, "CSSStyleDeclaration");
  let properties = $([]);
  let {debugCSSProperties} = libEnvironment;

  for (let [key, value] of (debugCSSProperties || [["display", "none"]])) {
    $style.setProperty(key, value, "important");
    properties.push([key, $style.getPropertyValue(key)]);
  }

  // Listen for changes to the style property and if our values are unset
  // then reset them.
  new MutationObserver(() => {
    for (let [key, value] of properties) {
      let propertyValue = $style.getPropertyValue(key);
      let propertyPriority = $style.getPropertyPriority(key);
      if (propertyValue != value || propertyPriority != "important")
        $style.setProperty(key, value, "important");
    }
  }).observe(element, {attributes: true,
                       attributeFilter: ["style"]});
  return true;
}

/**
 * Notifies the current contentScript that a new element has been hidden.
 * This is done by calling the globally available `checkElement` function
 * and passing the element.
 *
 * @param {HTMLElement} element The HTML element that was hidden.
 * @private
 */
function notifyElementHidden(element) {
  if (isExtensionContext && typeof checkElement === "function")
    checkElement(element);
}

/**
 * A callback function to be applied to a node.
 * @callback queryAndApplyCallback
 * @param {Node} node
 * @private
 */

/**
 * The query function. Accepts a callback function
 * which will be called for every node resulted from querying the document.
 * @callback queryAndApply
 * @param {queryAndApplyCallback} cb
 * @private
 */

/**
 * Given a CSS or Xpath selector, returns a query function.
 * @param {string} selector A CSS selector or a Xpath selector which must be
 * described with the following syntax: `xpath(the_actual_selector)`
 * @returns {queryAndApply} The query function. Accepts a callback function
 * which will be called for every node resulted from querying the document.
 * @private
 */
export function initQueryAndApply(selector) {
  let $selector = selector;
  if ($selector.startsWith("xpath(") &&
      $selector.endsWith(")")) {
    let xpathQuery = $selector.slice(6, -1);
    let evaluator = new $XPathEvaluator();
    let expression = evaluator.createExpression(xpathQuery, null);
    // do not use ORDERED_NODE_ITERATOR_TYPE or the test env will fail
    let flag = XPathResult.ORDERED_NODE_SNAPSHOT_TYPE;

    return cb => {
      if (!cb)
        return;
      let result = expression.evaluate(document, flag, null);
      let {snapshotLength} = result;
      for (let i = 0; i < snapshotLength; i++)
        cb(result.snapshotItem(i));
    };
  }
  return cb => $$(selector).forEach(cb);
}

/**
 * The query function. Retrieves all the nodes in the DOM matching the
 * provided selector.
 * @callback queryAll
 * @returns {Node[]} An array containing all the nodes in the DOM matching
 * the provided selector.
 * @private
 */

/**
 * Given a CSS or Xpath selector, returns a query function.
 * @param {string} selector A CSS selector or a Xpath selector which must be
 * described with the following syntax: `xpath(the_actual_selector)`
 * @returns {queryAll} The query function. Retrieves all the nodes in the DOM
 * matching the provided selector.
 * @private
 */
export function initQueryAll(selector) {
  let $selector = selector;
  if ($selector.startsWith("xpath(") &&
      $selector.endsWith(")")) {
    let queryAndApply = initQueryAndApply(selector);
    return () => {
      let elements = $([]);
      queryAndApply(e => elements.push(e));
      return elements;
    };
  }
  return () => Array.from($$(selector));
}

/**
 * Hides any HTML element or one of its ancestors matching a CSS selector if
 * it matches the provided condition.
 *
 * @param {function} match The function that provides the matching condition.
 * @param {string} selector The CSS selector that an HTML element must match
 *   for it to be hidden.
 * @param {?string} [searchSelector] The CSS selector that an HTML element
 *   containing the given string must match. Defaults to the value of the
 *   `selector` argument.
 * @param {?string} [onHideCallback] The callback that will be invoked after
 *    a HTML element was matched and hidden. It gets called with this element as
 *    argument.
 * @returns {MutationObserver} Augmented MutationObserver object. It has a new
 *   function mo.race added to it. This can be used by the snippets to
 *   disconnect the MutationObserver with the racing mechanism.
 *   Used like: mo.race(raceWinner(() => {mo.disconnect();}));
 * @private
 */
export function hideIfMatches(match, selector, searchSelector, onHideCallback) {
  if (searchSelector == null)
    searchSelector = selector;

  let won;
  const callback = () => {
    for (const element of $$(searchSelector)) {
      const closest = $(element).closest(selector);
      if (closest && match(element, closest)) {
        won();
        if (hideElement(closest) && typeof onHideCallback === "function")
          onHideCallback(closest);
      }
    }
  };
  return assign(
    new MutationObserver(callback),
    {
      race(win) {
        won = win;
        this.observe(document, {childList: true,
                                characterData: true,
                                subtree: true});
        callback();
      }
    }
  );
}

/**
 * Check if an element is visible
 *
 * @param {Element} element The element to check visibility of.
 * @param {CSSStyleDeclaration} style The computed style of element.
 * @param {?Element} closest The closest parent to reach.
 * @return {bool} Whether the element is visible.
 * @private
 */
export function isVisible(element, style, closest) {
  let $style = $(style, "CSSStyleDeclaration");
  if ($style.getPropertyValue("display") == "none")
    return false;

  let visibility = $style.getPropertyValue("visibility");
  if (visibility == "hidden" || visibility == "collapse")
    return false;

  if (!closest || element == closest)
    return true;

  let parent = $(element).parentElement;
  if (!parent)
    return true;

  return isVisible(parent, getComputedStyle(parent), closest);
}

/**
 * Returns the value of the `cssText` property of the object returned by
 * `getComputedStyle` for the given element.
 *
 * If the value of the `cssText` property is blank, this function computes the
 * value out of the properties available in the object.
 *
 * @param {Element} element The element for which to get the computed CSS text.
 *
 * @returns {string} The computed CSS text.
 * @private
 */
export function getComputedCSSText(element) {
  let style = getComputedStyle(element);
  let {cssText} = style;

  if (cssText)
    return cssText;

  for (let property of style)
    cssText += `${property}: ${style[property]}; `;

  return $(cssText).trim();
}
